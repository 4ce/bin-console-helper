@echo off
REM https://github.com/jeremejevs/cmdcolor utility
REM https://misc.flogisoft.com/bash/tip_colors_and_formatting#colors color codes


if /i "%1" EQU "entity" goto entity

if /i "%1" EQU "form" goto form

if /i "%1" EQU "migration" goto migration

if /i "%1" EQU "user" goto user

if /i "%1" EQU "schema" goto schema

if /i "%1" EQU "db" goto db

if /i "%1" EQU "sql" goto sql

if /i "%1" EQU "dql" goto dql

if /i "%1" EQU "auth" goto auth

if /i "%1" EQU "controller" goto controller

if /i "%1" EQU "migrate" goto migrate

if /i "%1" EQU "autowiring" goto autowiring

if /i "%1" EQU "ref" goto reference

if /i "%1" NEQ "" (
goto execute_unregistered_command
) else (
goto list
)

echo \033[93m Command not found | cmdcolor.exe
goto end

:reference
if /i "%2" EQU "" (
@php bin/console config:dump-reference
) else (
@php bin/console config:dump-reference "%~2"
)
goto end

:autowiring
if /i "%2" EQU "" (
@php bin/console debug:autowiring
) else (
@php bin/console debug:autowiring | findstr /i /r /N /A:02 /c:"^.*%2.*$"
)
goto end

:execute_unregistered_command
@php bin/console %*
goto end

:migrate
@php bin/console doctrine:migrations:migrate
goto end

:sql
if /i "%~2" EQU "" (
echo \033[93m Empty SQL query | cmdcolor.exe
) else (
@php bin/console doctrine:query:sql "%~2"
)
goto end

:dql
if /i "%~2" EQU "" (
echo \033[93m Empty DQL query | cmdcolor.exe
) else (
@php bin/console doctrine:query:dql "%~2"
)
goto end

:db
if /i "%2" EQU "create" (
@php bin/console doctrine:database:create
)
if /i "%2" EQU "drop" (
@php bin/console doctrine:database:drop
)
if /i "%2" EQU "import" (
@php bin/console doctrine:database:import
)
goto end

:schema
if /i "%2" EQU "create" (
@php bin/console doctrine:schema:create
)
if /i "%2" EQU "drop" (
@php bin/console doctrine:schema:drop %3
)
if /i "%2" EQU "update" (
@php bin/console doctrine:schema:update
)
if /i "%2" EQU "validate" (
@php bin/console doctrine:schema:validate
)
goto end

REM make:
:controller
@php bin/console make:controller
goto end

:user
@php bin/console make:user
goto end

:auth
@php bin/console make:auth
goto end

:migration
if /i "%2" EQU "g" (
@php bin/console doctrine:migrations:generate
) else (
@php bin/console make:migration
)
goto end

:form
@php bin/console make:form
goto end

:entity
if /i "%2" EQU "" (
@php bin/console make:entity
) else (
@php bin/console make:entity %2
)
goto end

:list
@php bin/console

:end